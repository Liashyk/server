<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>bot</title>
    <link href="../../resource/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<div style="text-align: center;">
    <h1>YOUR HTML PAGE HERE</h1>
</div>
<div style="position: absolute; left: 75%; right: 0; top: 50%; bottom: 52px; border: 2px solid #808080; border-radius: 6px;overflow-y: scroll;" id="scroll">
    <c:forEach items="${allChat}" var="bot">
        <blockquote class="blockquote-reverse text-info">
                ${bot.question}
        </blockquote>
        <blockquote class="text-warning">
                ${bot.answer}
        </blockquote>
    </c:forEach>
</div>
<c:url var="addAction" value="/add"/>
<form:form action="${addAction}" commandName="newChat">
    <form:input type="text" path="question" class="form-control"
           style="position: absolute; right: 100px; bottom: 5px; width: 255px; border: 2px solid #808080; border-radius: 6px;"/>
    <input type="submit" class="btn btn-primary" style="bottom: 5px; position: absolute; right: 0; ">
</form:form>
</body>
<script>
    window.onload = function(){
        var scrollinDiv = document.getElementById('scroll');
        scrollinDiv.scrollTop = 9999;
        setInterval(function() {

        }, 100);
    }
</script>
</html>
