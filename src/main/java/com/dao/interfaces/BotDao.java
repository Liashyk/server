package com.dao.interfaces;

import com.model.Bot;

import java.util.List;

public interface BotDao {

    public List<Bot> getChat();

    public void setToChat(Bot bot);

}
