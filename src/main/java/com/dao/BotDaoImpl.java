package com.dao;

import com.dao.interfaces.BotDao;
import com.model.Bot;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BotDaoImpl implements BotDao {
    private static final Logger logger = LoggerFactory.getLogger(BotDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Bot> getChat() {
        Session session = sessionFactory.getCurrentSession();
        List<Bot> botList = session.createQuery("from Bot").getResultList();
        return botList;
    }

    @Override
    public void setToChat(Bot bot) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(bot);
    }
}